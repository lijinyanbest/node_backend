const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const  app = express();

const http = require('http');
const server = http.Server(app);
const socketIO = require('socket.io');
const io = socketIO(server);

const users = {};

io.on('connection', (socket) => {
    
    socket.on('new-user', name => {
        users[socket.id] = name;
        socket.broadcast.emit('user-connected', name);
    });

    socket.on('new-message', message => {
        socket.broadcast.emit('new-broadcast', {message:message, name:users[socket.id]});
    });

    socket.on('disconnect', () => {
        console.log(`users[socket.id] disconnected`);
        socket.broadcast.emit('user-disconnected', users[socket.id])
        delete users[socket.id]
    });
});


var corsOptions = {origin:"http://localhost:4200"};

app.use(cors(corsOptions));

app.use('/',express.static("resources/static/assets/tutorial_uploads/"));
app.use('/',express.static("resources/static/assets/tutorial_resized/"));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse request of content-type application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:true}));


const db = require("./app/models");

db.sequelize.sync();

// force: true will drop the table if it already exists
// db.sequelize.sync({force: true}).then(() => {
//   console.log('Drop and Resync Database with { force: true }');
//   initial();
// });


// simple route
app.get("/",(req,res) => {
    res.json({message:"Welcome to application."});
});

//routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/turorial.routes')(app);


//set port, listen for request
const PORT = process.env.PORT || 8080;

server.listen(PORT ,() => {
    console.log(`Server is running on port ${PORT}.`);
});


