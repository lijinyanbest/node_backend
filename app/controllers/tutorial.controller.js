const db = require("../models");
const fs = require("fs");
const path = require('path');

const Tutorial = db.tutorials;
const Comment = db.comments;
// const Image = db.images;
const Op = db.Sequelize.Op;

const sharp = require('sharp');

//create and save a new tutorial
exports.create = (req, res) => {
    //validate request
    if(!req.body.title){
        res.status(400).send({
            message:"Content can not be empty!"
        });
        return;
    }

    const filename = req.file.originalname.replace(/\..+$/, "");
    const newFilename = `best-${filename}-${Date.now()}.jpeg`;

    sharp(req.file.path)
        // .resize(300)
        .resize(250,250)
        .toFormat("jpeg")
        .jpeg({ quality: 50 })
        .toFile(`resources/static/assets/tutorial_resized/${newFilename}`);


    //create a tutorial
    const tutorial = {
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published:false,
        // image:req.file.path,
        image: newFilename,
        imageUrl: req.file.filename
    };

    // save tutorial in the database
    Tutorial.create(tutorial).then(data => {res.send(data);})
    .catch(err => {res.status(500).send({
        message: err
         });
    });
};


exports.createComments = (req, res) => {

    Comment.create({
        name: req.body.name,
        text: req.body.text,
        tutorialId: req.body.tutorialid,
    }).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

exports.deleteComment = (req, res ) => {
    const id = req.params.id;
    Comment.destroy({where:{id:id}}).then(comment => {
        if(comment ==1 ){
            res.send({
                message:"comment has been delete successfully!"
            });
        }else{
            res.send({
                message:`Cannot delete Tutorial with id=${id}. Maybe Turorial was not found or req.body id empty!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: "Could not delete Tutorial with id=" + id
        });
    });
};

exports.findComments = (req, res) => {
    const id = req.params.id;

    Comment.findByPk(id,{include: ["tutorials"]}).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message:"Erroe retrieving tutorial with id = " + id
        });
    });
};

exports.getallCommetns = (req, res) => {
    Comment.findAll().then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

//Retrieve all Tutorial from the database.
exports.findAll = (req, res) => {
    const title = req.query.title;
    var condition = title ? {title: {[Op.like]: `%${title}%`}} : null;

    Tutorial.findAll({where: condition}, { include: ["comments"]}).then(data => {
        const tutorials = data.map( da => {
           return tutorial = {
                id: da.id,
                title: da.title,
                description: da.description,
                published: da.published,
                updatedAt: da.updatedAt,
                // image:req.file.path,
                image: "http://localhost:8080/" + da.image,
                imageUrl: "http://localhost:8080/" + da.imageUrl,
            };

        });
        res.send(tutorials);
    }).catch(err => {
        res.status(500).send({
            message:err.message
        });
    });
};

//Find a single Tutorial with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Tutorial.findByPk(id, { include: ["comments"]}).then(data => {
        const tutorial = {
            id: data.id,
            title: data.title,
            description: data.description,
            published: data.published,
            // image:req.file.path,
            image: "http://localhost:8080/" + data.image,
            imageUrl: "http://localhost:8080/" + data.imageUrl,
            updatedAt: data.updatedAt,
            comment:data.comments
        }
        res.send(tutorial);
    }).catch(err => {
        res.status(500).send({
            message: "Error retrieving Tutorial with id=" + id
        });
    });
};

//Update a Tutorial by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    var tutorial={};

    if(req.file){

        const filename = req.file.originalname.replace(/\..+$/, "");
        const newFilename = `best-${filename}-${Date.now()}.jpeg`;

        sharp(req.file.path)
            // .resize(300)
            .resize(250,250)
            .toFormat("jpeg")
            .jpeg({ quality: 50 })
            .toFile(`resources/static/assets/tutorial_resized/${newFilename}`);

         //create a tutorial
        tutorial = {
            title: req.body.title,
            description: req.body.description,
            published: req.body.published ? req.body.published:false,
            // image:req.file.path,
            image: newFilename,
            imageUrl: req.file.filename
        };
    }else{
          //create a tutorial
        tutorial = {
            title: req.body.title,
            description: req.body.description,
            published: req.body.published ? req.body.published:false,
        };
    }

    Tutorial.update(tutorial, {where:{id:id}})
    .then(num => {
        if (num == 1 ){
            res.send({
                message: "Tutorial was updated sucessfully."
            });
        }else{
            res.send({
                message:`Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message:"Error updating Tutorial with id=" + id
        });
    });
};

//delete a tutorial with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    
    Tutorial.destroy({where:{id:id}}).then(num =>{
        if(num == 1){
            res.send({
                message:"Tutorial was deleted successfully!"
            });
        }else{
            res.send({
                message:`Cannot delete Tutorial with id=${id}. Maybe Turorial was not found or req.body id empty!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message:"Could not delete Tutorial with id=" + id
        });
    });
};

//delete all tutorial from the database.
exports.deleteAll = (req, res) => {
    Tutorial.destroy({
        where:{}, 
        truncate:false
    })
    .then(nums => {
        const direct = "resources/static/assets/tutorial_resized/";
        fs.readdir(direct,(err, files) => {
            if(err) throw err;
            for(const file of files){
                fs.unlink(path.join(direct,file), err => {
                    if (err) throw err;
                });
            }
        });

        const dirctOrigi = "resources/static/assets/tutorial_uploads/";
        fs.readdir(dirctOrigi, (err, files) => {
            if(err) throw err;
            for(const file of files){
                fs.unlink(path.join(dirctOrigi,file), err => {
                    if (err) throw err;
                });
            }
        });


        res.send({
            message:`${nums} Tutorials was deleteed successfully!`
        });
    })
    .catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};

//find all published tutorials
exports.findAllPublished = (req, res) => {
    Tutorial.findAll({where:{published:true} })
    .then(data => {
        const tutorials = data.map( da => {
            return tutorial = {
                 id: da.id,
                 title: da.title,
                 description: da.description,
                 published: da.published,
                 updatedAt: da.updatedAt,
                 // image:req.file.path,
                 image: "http://localhost:8080/" + da.image,
                 imageUrl: "http://localhost:8080/" + da.imageUrl,
             };
 
         });
         res.send(tutorials);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};
