const db = require("../models");
const config = require("../config/auth.config");
const { user } = require("../models");
const Op = db.Sequelize.Op;
const User = db.user;
const Role = db.role;

exports.allAccess = (req, res) => {
    res.status(200).send("Public Content.");
};

exports.userBoard = (req, res) => {
    res.status(200).send("User Content");
};

exports.adminBoard = (req, res) => {

    User.findAll().then(users => {
        const userall={};
        for(let i=0; i < users.length; i++ ){
            var authorities = [];
            users[i].getRoles().then(roles => {
                authorities.push(roles[i].name.toUpperCase());
            });
            res.send({
                id: users[i].id,
                username: users[i].username,
                email: users[i].email,
                roles: authorities
            });
        }

    }).catch(err => {
        res.status(500).send({
            message:err.message
        });
    });
    // res.status(200).send("Admin Content.");
};

exports.moderatorBoard = (req, res) => {
    User.findAll({include:[
        {model: Role,
        as:"roles",
        arrtibutes:["name"],
        through:{
            attributes:["name"],
        }
        },
    ],}).then(users => {
        res.send(users);

    }).catch(err => {
        res.status(500).send({
            message:err.message
        });
    });
};