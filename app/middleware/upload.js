const multer = require("multer");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "resources/static/assets/tutorial_uploads/");
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-best-${file.originalname}`);
  },
});

const imageFilter = (req, file, cb) => {
  console.log("upload:" + file)
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb("Please upload only images.", false);
  }
};

const resizeImage = async(req, file, cb) => {

};



const uploadFile = multer({ storage: storage, fileFilter: imageFilter });
module.exports = {
  uploadFile:uploadFile,
  resizeImage:resizeImage
};