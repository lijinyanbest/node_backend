const tutorials = require("../controllers/tutorial.controller");
var router = require("express").Router();
const {uploadFile} = require("../middleware");

module.exports = function(app){


    app.use(function(req,res,next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    //create a new tutorial
    router.post("/", [uploadFile.uploadFile.single('image')], tutorials.create);

    router.post("/comment", tutorials.createComments);

    router.get("/comment/:id",tutorials.findComments)
    router.delete("/comment/:id",tutorials.deleteComment)

    //retrieve all tutorials
    router.get("/", tutorials.findAll);

    //retrieve all published tutorials
    router.get("/published", tutorials.findAllPublished)

    //retrieve a single tutorial with id
    router.get("/:id", tutorials.findOne);

    //update a tutorial with id
    router.put("/:id",[uploadFile.uploadFile.single('image')],tutorials.update);

    //delete a tutorial with id
    router.delete("/:id", tutorials.delete);

    //delete all tutorials
    router.delete("/",tutorials.deleteAll);

    app.use('/api/tutorials',router);
    
};