const config = require("../config/db.config.js");

const Sequelize = require("sequelize");

const sequelize = new Sequelize(
    config.DB,
    config.USER,
    config.PASSWORD,
    {
        host:config.HOST,
        dialect:config.dialect,
        operatorsAliases:false,
    pool:{
        max:config.pool.max,
        min:config.pool.min,
        acquire:config.pool.acquire,
        idle:config.pool.idle
    }
    }
); 

const db ={};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);

db.role.belongsToMany(db.user,{
    through:"users_rolesnode",
    foreignKey:"roleId",
     // as:"rolesnodes",
    otherKey:"userId"
});

db.user.belongsToMany(db.role,{
    through:"users_rolesnode",
    foreignKey:"userId",
    // as:"rolesnodes",
    otherKey:"roleId"
    
});

db.ROLES = ["user","admin","moderator"];

db.tutorials = require("../models/tutorial.model.js")(sequelize, Sequelize);
db.comments = require("../models/comment.model.js")(sequelize,Sequelize);
db.images = require("../models/image.model.js")(sequelize, Sequelize);

//one to many
db.tutorials.hasMany(db.comments, {as: "comments"});
db.comments.belongsTo(db.tutorials, {
    foreignKey: "tutorialId",
    as: "tutorials"
});


module.exports = db;