module.exports = (sequelize, Sequelize) => {
    const Image =  sequelize.define("images", {
        imagetype:{
            type:Sequelize.STRING
        },
        imagename:{
            type:Sequelize.STRING
        },
        imagedata:{
            type:Sequelize.BLOB("long")
        }
    });

    return Image;
}

